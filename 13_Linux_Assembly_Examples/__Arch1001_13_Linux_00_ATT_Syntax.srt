1
00:00:00,24 --> 00:00:00,51
all right,

2
00:00:00,51 --> 00:00:02,79
So let's talk a little more deeply about the difference

3
00:00:02,79 --> 00:00:06,67
between the Intel syntax of X 86 disassembly,

4
00:00:06,67 --> 00:00:08,41
which is what I've been showing you thus far,

5
00:00:08,42 --> 00:00:10,26
and the AT and T syntax,

6
00:00:10,26 --> 00:00:12,86
which is much more common than UNIX based systems

7
00:00:13,54 --> 00:00:14,27
So my classes,

8
00:00:14,27 --> 00:00:14,62
of course,

9
00:00:14,62 --> 00:00:16,15
prefer Intel syntax

10
00:00:16,19 --> 00:00:19,75
But originally I actually learned AT and T syntax as

11
00:00:19,75 --> 00:00:23,52
part of the computer architecture class that used the bomb

12
00:00:23,52 --> 00:00:26,05
lab example that we're going to be talking about

13
00:00:26,64 --> 00:00:29,47
But originally I learned AT and T Syntax as part

14
00:00:29,47 --> 00:00:33,66
of the computer architecture class that utilized this binary bomb

15
00:00:33,66 --> 00:00:35,89
example that we're going to be using later on in

16
00:00:35,89 --> 00:00:36,47
the class

17
00:00:36,48 --> 00:00:39,21
So I personally believe that it's important to know if

18
00:00:39,21 --> 00:00:43,41
both sin taxes it's not particularly difficult to translate between

19
00:00:43,41 --> 00:00:43,65
them

20
00:00:44,04 --> 00:00:46,26
It's just another thing that you have to remember

21
00:00:46,84 --> 00:00:50,38
I believe this is important because depending on the security

22
00:00:50,38 --> 00:00:52,63
researchers that you're watching a talk for,

23
00:00:52,64 --> 00:00:55,09
you may be seeing it in Intel syntax or AT

24
00:00:55,09 --> 00:00:56,05
and T syntax

25
00:00:56,44 --> 00:00:58,29
And if you can't read one of them,

26
00:00:58,29 --> 00:00:59,74
then you're not going to be able to understand what

27
00:00:59,74 --> 00:01:00,25
they're saying

28
00:01:00,94 --> 00:01:03,0
And part of the whole goal of this class is

29
00:01:03,0 --> 00:01:05,44
to help you understand how things work

30
00:01:05,59 --> 00:01:08,4
So the brief history of AT and T syntax is

31
00:01:08,4 --> 00:01:11,31
that Bell Labs created multi IX,

32
00:01:11,31 --> 00:01:12,76
which ran on mainframes

33
00:01:12,76 --> 00:01:15,42
Malt IX became the inspiration for UNIX,

34
00:01:15,42 --> 00:01:16,69
which ran on PDP

35
00:01:16,69 --> 00:01:20,72
Elevens was primarily written in assembly until the same folks

36
00:01:20,72 --> 00:01:25,11
who invented UNIX invented C which point they rewrote some

37
00:01:25,11 --> 00:01:28,19
of the unique stuff in C and then that made

38
00:01:28,19 --> 00:01:29,14
it much more portable

39
00:01:29,14 --> 00:01:32,53
Two different architectures and so unique thing eventually got ported

40
00:01:32,53 --> 00:01:36,58
to 80 86 with C and a bit of assembly

41
00:01:36,59 --> 00:01:41,16
But that assembly syntax was adapted from the syntax used

42
00:01:41,16 --> 00:01:42,36
for the PDP 11

43
00:01:43,04 --> 00:01:44,01
And then finally,

44
00:01:44,02 --> 00:01:45,06
there is good news,

45
00:01:45,06 --> 00:01:45,77
not UNIX,

46
00:01:45,77 --> 00:01:49,19
with Richard Stallman flipping a table because we're going to

47
00:01:49,19 --> 00:01:52,06
be citing a lot of documentation from the canoe assembler

48
00:01:52,74 --> 00:01:55,31
So Intel syntax is what's preferred on windows,

49
00:01:55,31 --> 00:01:57,39
and you can think of it sort of like algebra

50
00:01:57,39 --> 00:02:00,35
where you have the things on the right to get

51
00:02:00,35 --> 00:02:01,86
moved into the thing on the left,

52
00:02:02,34 --> 00:02:05,94
sources and destinations get moved to a destination on the

53
00:02:05,94 --> 00:02:06,26
left

54
00:02:06,94 --> 00:02:10,28
So here you have no idea which way this goes

55
00:02:10,28 --> 00:02:13,46
but because we can tell it's Intel syntax because it

56
00:02:13,46 --> 00:02:14,95
doesn't have these decorators,

57
00:02:14,95 --> 00:02:16,06
these percent signs

58
00:02:16,54 --> 00:02:18,33
Then we have to assume that it's going to be

59
00:02:18,33 --> 00:02:19,96
going from right to left

60
00:02:20,74 --> 00:02:21,5
Same thing

61
00:02:21,5 --> 00:02:22,7
Add it is,

62
00:02:22,88 --> 00:02:26,06
this is both a source and destination plus 14 back

63
00:02:26,06 --> 00:02:26,95
into the destination

64
00:02:27,44 --> 00:02:27,65
Okay,

65
00:02:28,34 --> 00:02:29,78
AT and T Syntex,

66
00:02:29,78 --> 00:02:32,04
which is preferred on most UNIX systems

67
00:02:32,07 --> 00:02:33,47
It's more like elementary school

68
00:02:33,47 --> 00:02:34,66
One plus two equals three

69
00:02:34,66 --> 00:02:38,1
The operations on the left to go to the right

70
00:02:38,11 --> 00:02:40,86
Sources and destinations move from left to right

71
00:02:40,87 --> 00:02:42,2
So here it is,

72
00:02:42,2 --> 00:02:48,3
RSP being moved into our VP and RSP plus 14

73
00:02:48,3 --> 00:02:49,36
back into RSP

74
00:02:49,84 --> 00:02:50,0
Now,

75
00:02:50,0 --> 00:02:50,62
from this,

76
00:02:50,62 --> 00:02:53,18
you can see that there is this decoration in terms

77
00:02:53,18 --> 00:02:57,51
of registers getting percent signs and immediate values getting dollar

78
00:02:57,51 --> 00:02:58,06
signs

79
00:02:58,84 --> 00:03:02,32
Another big difference from Intel syntax is that whereas Intel

80
00:03:02,32 --> 00:03:07,07
syntax indicates the size of memory accesses with things like

81
00:03:07,08 --> 00:03:07,57
keyword,

82
00:03:07,57 --> 00:03:08,13
pointer,

83
00:03:08,32 --> 00:03:09,22
AT and T,

84
00:03:09,22 --> 00:03:13,85
syntax indicates the size with a change to the pneumonic

85
00:03:13,85 --> 00:03:14,73
of the instruction

86
00:03:14,79 --> 00:03:16,83
So instead of just move its move,

87
00:03:16,84 --> 00:03:18,74
be to indicate that it's moving a bite

88
00:03:18,74 --> 00:03:21,86
Move W to indicate it's moving a word Move l

89
00:03:21,86 --> 00:03:23,46
to indicate it's moving along,

90
00:03:23,46 --> 00:03:27,51
which an Intel parlance is the D word move Q

91
00:03:27,51 --> 00:03:28,62
for a Quad Word,

92
00:03:28,62 --> 00:03:31,75
which is secured the same as before of the key

93
00:03:31,75 --> 00:03:35,2
little bit here is that it says only when this

94
00:03:35,2 --> 00:03:36,66
is from this documentation

95
00:03:36,67 --> 00:03:39,77
It says only when no other way to dis ambiguity

96
00:03:39,78 --> 00:03:41,18
the instruction exists,

97
00:03:41,2 --> 00:03:44,44
so sometimes things could just be moved instead of move

98
00:03:44,44 --> 00:03:46,35
W move instead of move l

99
00:03:46,74 --> 00:03:49,79
And it sort of becomes incumbent upon the reader to

100
00:03:49,79 --> 00:03:52,12
understand what the size is at that point

101
00:03:52,13 --> 00:03:52,73
Furthermore,

102
00:03:52,73 --> 00:03:55,84
some mnemonics have been more or less renamed just to

103
00:03:55,84 --> 00:03:59,46
use this sort of VW lq naming convention

104
00:03:59,84 --> 00:04:03,25
But these are not necessarily just strict suffixes

105
00:04:03,35 --> 00:04:06,08
These are things like the CWD,

106
00:04:06,08 --> 00:04:07,54
which is one of those instructions I said

107
00:04:07,54 --> 00:04:08,98
We don't really care about that much,

108
00:04:08,98 --> 00:04:11,63
but it was convert word to D word through sign

109
00:04:11,63 --> 00:04:17,62
extension that turns into convert word too long or something

110
00:04:17,62 --> 00:04:20,46
like move S X for sign extension

111
00:04:20,84 --> 00:04:24,04
The X gets dropped and it turns into move sign

112
00:04:24,04 --> 00:04:25,78
extension bite

113
00:04:25,86 --> 00:04:29,7
Two words size And I personally really don't like this

114
00:04:29,7 --> 00:04:31,68
because it means that if you're trying,

115
00:04:31,68 --> 00:04:33,86
you're looking at AT and t syntax and you're trying

116
00:04:33,86 --> 00:04:34,4
to go fine

117
00:04:34,4 --> 00:04:34,72
Okay

118
00:04:34,72 --> 00:04:37,44
I've never seen this move s and VW instruction before

119
00:04:37,44 --> 00:04:38,05
What is it?

120
00:04:38,44 --> 00:04:38,97
You go

121
00:04:38,97 --> 00:04:40,82
You look at the manual and there is no move

122
00:04:40,82 --> 00:04:44,85
Sp w so makes it a lot harder to look

123
00:04:44,85 --> 00:04:45,88
things up in the manual

124
00:04:45,91 --> 00:04:46,5
Same thing,

125
00:04:46,5 --> 00:04:47,65
especially with this

126
00:04:48,14 --> 00:04:50,41
So just a quick example of this from some real

127
00:04:50,41 --> 00:04:53,77
code will Look at later can see that over here

128
00:04:53,77 --> 00:04:56,29
it's move s l two Q

129
00:04:56,29 --> 00:04:57,94
So long to cured

130
00:04:57,94 --> 00:04:59,99
You can see we've got a long here,

131
00:04:59,99 --> 00:05:03,63
a 32 bit value and a Q word 16,

132
00:05:03,64 --> 00:05:05,06
64 bit value here,

133
00:05:05,44 --> 00:05:08,63
and same thing over here is move S x D

134
00:05:08,64 --> 00:05:11,69
Because there is actually a move SX and move s

135
00:05:11,69 --> 00:05:15,1
xD when it's operating on D word sizes extended to

136
00:05:15,11 --> 00:05:15,96
keyword sizes

137
00:05:16,64 --> 00:05:18,85
And then one of the things that I think is

138
00:05:18,85 --> 00:05:19,49
quite frankly,

139
00:05:19,49 --> 00:05:22,46
the worst about AT and T syntax is that this

140
00:05:22,46 --> 00:05:27,03
base plus index time scale plus displacement representation gets turned

141
00:05:27,03 --> 00:05:27,95
into base,

142
00:05:27,95 --> 00:05:29,94
plus index time,

143
00:05:29,94 --> 00:05:31,9
scale plus displacement

144
00:05:31,91 --> 00:05:34,53
But you have no idea what the operations are actually

145
00:05:34,53 --> 00:05:35,22
between here

146
00:05:35,22 --> 00:05:36,84
So if you were just seeing this for the first

147
00:05:36,84 --> 00:05:37,09
time,

148
00:05:37,09 --> 00:05:40,29
you had no idea that there's some specific type of

149
00:05:40,29 --> 00:05:41,25
math going on

150
00:05:41,84 --> 00:05:44,17
So this becomes just another thing that you have to

151
00:05:44,17 --> 00:05:45,44
memorize and recognize

152
00:05:45,44 --> 00:05:45,76
Okay,

153
00:05:45,76 --> 00:05:46,76
If I see you know,

154
00:05:47,14 --> 00:05:49,03
to operate a ends well,

155
00:05:49,25 --> 00:05:52,41
is it based plus index time scale plus one?

156
00:05:52,41 --> 00:05:54,25
Or is it index time scale?

157
00:05:54,74 --> 00:05:56,95
So it gets a little more difficult,

158
00:05:56,96 --> 00:06:00,26
but usually it's relatively clear

159
00:06:00,84 --> 00:06:01,53
But later on,

160
00:06:01,53 --> 00:06:03,42
once we learn how to read the manual,

161
00:06:03,42 --> 00:06:06,28
it will become a little more clear how to disambiguate

162
00:06:06,28 --> 00:06:07,86
it if there's ambiguity

163
00:06:08,34 --> 00:06:11,22
So some examples of this is that you might have

164
00:06:11,23 --> 00:06:12,6
in Intel syntax,

165
00:06:12,61 --> 00:06:14,02
call Cured pointer

166
00:06:14,02 --> 00:06:17,31
And then it's calculating up this R BX plus RS

167
00:06:17,31 --> 00:06:19,6
items for minus E eight,

168
00:06:19,61 --> 00:06:22,55
whereas in AT and T Syntax E eight,

169
00:06:22,56 --> 00:06:26,52
the displacement gets moved outside of the parentheses and it

170
00:06:26,52 --> 00:06:28,88
just becomes our BX RC four,

171
00:06:28,88 --> 00:06:31,1
and you need to just know that that is multiplied

172
00:06:31,1 --> 00:06:31,86
And that is added,

173
00:06:32,44 --> 00:06:33,03
same thing,

174
00:06:33,03 --> 00:06:34,35
going the opposite direction

175
00:06:34,35 --> 00:06:36,32
So if we had a keyword pointer instead,

176
00:06:36,32 --> 00:06:37,74
that's going to turn into a move

177
00:06:37,74 --> 00:06:38,16
Q

178
00:06:38,54 --> 00:06:41,39
And RVP plus eight is the destination,

179
00:06:41,39 --> 00:06:46,48
so our x percent sign for register into rvp percent

180
00:06:46,48 --> 00:06:48,05
sign for Register plus eight

181
00:06:49,14 --> 00:06:51,51
Same thing here with Elia again,

182
00:06:51,52 --> 00:06:53,92
basically just taking the square brackets,

183
00:06:53,92 --> 00:06:55,53
turning into parentheses,

184
00:06:55,53 --> 00:06:56,76
three parameters on the inside,

185
00:06:56,76 --> 00:06:59,85
one displacement on the outside and flipping the order

186
00:07:00,34 --> 00:07:02,06
So if we go back and look at that simple

187
00:07:02,06 --> 00:07:04,7
assembly that we're going to see later on,

188
00:07:04,71 --> 00:07:06,73
we have a situation where there's a little bit of

189
00:07:06,73 --> 00:07:09,52
ambiguity here in terms of okay,

190
00:07:09,52 --> 00:07:09,85
move

191
00:07:09,85 --> 00:07:10,8
L Well,

192
00:07:10,8 --> 00:07:14,67
that's telling you specifically that this f f should be

193
00:07:14,72 --> 00:07:15,95
long sized

194
00:07:15,96 --> 00:07:18,59
So that's a D word being moved into

195
00:07:18,6 --> 00:07:19,06
Sorry

196
00:07:19,07 --> 00:07:19,55
Yeah,

197
00:07:19,56 --> 00:07:20,16
that's right

198
00:07:20,44 --> 00:07:20,76
Okay,

199
00:07:21,04 --> 00:07:23,28
double checking my syntax as I'm going back and forth

200
00:07:23,29 --> 00:07:23,58
Yes,

201
00:07:23,59 --> 00:07:25,12
18 Syntex on this side

202
00:07:25,15 --> 00:07:29,78
Probably label that to the so this is moving a

203
00:07:29,78 --> 00:07:34,8
long so at 0000 f f f sorry ff into

204
00:07:34,83 --> 00:07:36,7
this memory location

205
00:07:36,71 --> 00:07:40,25
But right here we have moved from some memory location

206
00:07:40,26 --> 00:07:41,36
into a register

207
00:07:41,84 --> 00:07:43,53
But how do we know that this,

208
00:07:43,54 --> 00:07:44,32
you know,

209
00:07:44,33 --> 00:07:47,3
is actually moving a quad word like How do we

210
00:07:47,3 --> 00:07:49,47
know it's right over here?

211
00:07:49,47 --> 00:07:50,4
We see it's a quad word,

212
00:07:50,4 --> 00:07:52,06
but it's just moving from memory

213
00:07:52,06 --> 00:07:53,08
How do we know the size?

214
00:07:53,08 --> 00:07:54,11
We don't know whether it's a bite

215
00:07:54,11 --> 00:07:54,92
Whether it's two bites,

216
00:07:54,92 --> 00:07:55,68
whether it's four bites,

217
00:07:55,68 --> 00:07:59,38
whether it's eight bytes So this is one of those

218
00:07:59,38 --> 00:08:00,4
things where again,

219
00:08:00,4 --> 00:08:01,7
I wish they would be consistent

220
00:08:01,7 --> 00:08:03,77
Just use it all the time instead of saying,

221
00:08:03,77 --> 00:08:04,1
Oh yeah,

222
00:08:04,11 --> 00:08:04,96
it's not ambiguous

223
00:08:05,34 --> 00:08:05,84
Well,

224
00:08:05,85 --> 00:08:09,0
it's ambiguous to you because you don't yet know how

225
00:08:09,0 --> 00:08:09,87
to read the manual,

226
00:08:09,87 --> 00:08:12,6
and therefore you can't look behind the scenes to see

227
00:08:12,6 --> 00:08:14,67
exactly how this instruction was encoded

228
00:08:14,68 --> 00:08:15,7
So unfortunately,

229
00:08:15,7 --> 00:08:16,35
for now,

230
00:08:16,36 --> 00:08:19,52
when we're going through these AT and T syntax examples

231
00:08:19,52 --> 00:08:21,08
there might be a little bit of ambiguity

232
00:08:21,08 --> 00:08:23,53
But hopefully we'll clear that up later on when you

233
00:08:23,53 --> 00:08:25,16
learn to read the fund manual

